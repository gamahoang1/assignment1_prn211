﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exercise2.DAL
{
    internal class DAO
    {
        private static IConfiguration _configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json", true, true).Build();

        public static SqlConnection GetConnection()
        {
            string ConnentionStr = _configuration.GetConnectionString("DbConnection");
            return new SqlConnection(ConnentionStr);
        }

        public static DataTable GetDataBySql(string sql, params SqlParameter[] parameters)// params chi duoc chuyen vao 1 lan va phai o sau cung
        {
            SqlCommand command = new SqlCommand(sql, GetConnection());
            if (parameters != null || parameters.Length != 0)
            {

                command.Parameters.AddRange(parameters);
            }
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            return dt;

        }
        public static int ExecuteBySql(string sql, params SqlParameter[] parameters)
        {
            SqlCommand cmd = new SqlCommand(sql, GetConnection());
            if (parameters != null || parameters.Length != 0)
            {
                cmd.Parameters.AddRange(parameters);
            }
            cmd.Connection.Open();
            int count = cmd.ExecuteNonQuery();
            cmd.Connection.Close();
            return count;
        }

    }
}
